// console.log("Hello")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function welcomeMessage() {

		let userName = prompt("What is your Name?");
		let userAge = prompt("What is your age?");
		let userAddress = prompt("Where do you live?");

		console.log("Hello, " + userName);
		console.log("You are " + userAge + " years old.");
		console.log("You live in " + userAddress);
		alert("Thank you for your input!")

	}

	welcomeMessage();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printFavoriteBandsOrMusicalArtist() {

		console.log("1. Ben and Ben");
		console.log("2. Parokya ni Edgar");
		console.log("3. Imagine Dragons");
		console.log("4. Eminem");
		console.log("5. Kanye West");
	}

	printFavoriteBandsOrMusicalArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printFavoriteMoviesWithRatings() {

		let FaveMovie1 = "Shawshank Redemption";
		let FaveMovie2 = "The Dark Knight";
		let FaveMovie3 = "Pursuit of Happyness";
		let FaveMovie4 = "The Dark Knight Rises";
		let FaveMovie5 = "Joker";

		console.log("1. " + FaveMovie1 + " \nRotten Tomatoes Rating: 91%")
		console.log("2. " + FaveMovie2 + " \nRotten Tomatoes Rating: 94%")
		console.log("3. " + FaveMovie3 + " \nRotten Tomatoes Rating: 67%")
		console.log("4. " + FaveMovie4 + " \nRotten Tomatoes Rating: 87%")
		console.log("5. " + FaveMovie5 + " \nRotten Tomatoes Rating: 68%")
	}

	printFavoriteMoviesWithRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: ");
	console.log(friend1);
	console.log(friend2);
	console.log(friend3);
	
};

printFriends();
